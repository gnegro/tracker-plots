title: 'Strip Hit efficiency as a function of the instantaneous luminosity'
caption: 'The hit efficiency is computed from this selection:

$\bullet$ Use tracks from the iterative tracking passing the "highPurity" selection;

$\bullet$ To avoid inactive regions such as the bonding region for modules with two sensors, and to take account of any residual misalignment, trajectories passing near the edges of sensors or their readout electronics in the studied layer are excluded from consideration. The efficiency is determined from the fraction of traversing tracks with a hit anywhere in the non-excluded region of a traversed module within a range of 15 strips;

$\bullet$ If the trajectory starts or ends in a considered module, it is discarded for computing the efficiency of the module. Thus, measurement in the first layer, TIB layer 1, relies on pixel seeding and moreover no measurement is possible in the outermost layers, ie TOB layer 6 and TEC disk 9;

$\bullet$ Known bad modules are excluded from the measurement. Moreover, the few modules with low efficiency (upper limit on the efficiency lower than the average layer efficiency minus 10%) are not included the average efficiency to avoid biases. 

For double layers, mono and stereo sensors are used for the computation ; the average efficiency is shown.
Interpretation of the results:

$\bullet$ The hit efficiency scales linearly with both the instantaneous luminosity and the pileup.

$\bullet$ The hit efficiency, greater than 98% even at high pileup, depends on the layer.

$\bullet$ As the inefficiency mainly depends on the particle flux, inner layers are more sensitive than the outer ones.

$\bullet$ Moreover, the inefficiency depends on the sensitive volume under the strip and thus depends at first order on the sensor thickness and to a less extent, on the pitch.

The following figures are displayed, for all of them data from the long LHC fill 6714 (14 hours) taken in 2018 have been used. The number of colliding bunch crossing (nBx) is 2544 and the peak pileup is 53.1.:

$\bullet$ Hit efficiency of Silicon Strip detectors from the TIB Layer 4 as a function of the instantaneous luminosity. 

$\bullet$ Hit efficiency of Silicon Strip detectors from the TOB Layer 1 as a function of the instantaneous luminosity. 

$\bullet$ Hit efficiency of Silicon Strip detectors from the 4 TIB layers as a function of the instantaneous luminosity.

$\bullet$ Hit efficiency of Silicon Strip detectors from the 5 TOB layers as a function of the instantaneous luminosity.'
date: '2018-09-12'
tags:
- Run-2
- Strips
- pp
- Collisions
- 2018