title: 'Cluster Charge Data/MC comparison'
caption: 'In late 2015 and early 2016, the Silicon Strip Tracker observed a decrease in signal-to-noise, and an associated loss of hits on tracks. This behaviour was caused by saturation effects in the pre-amplifier of the APV25 readout chip. The drain speed of the pre-amplifier was affected more strongly by the change in operating temperature than anticipated, leading to a very slow discharge of the amplifier with a decay constant O(15$\mus$), that become apparent under high occupancy conditions. During this period, referred to as "old APV settings", about 20$fb^{-1}$ of 2016 data were affected. The drain speed was changed on the 13$^{th}$ August 2016 to allow for faster recovery of the pre-amplifier, and recovery of the hit efficiency to the same level as in Run-1.

In order to improve the description of 2016 data in the period affected by the APV saturation issue, the simulation of the Silicon Strip Tracker includes a dedicated description of the APV25 chip dynamic gain. A set of APV baseline distributions, which represent the charge accumulated on a strip over several bunch crossings, are provided as an input, separately per layer, and in bins of PU and z within a layer. Each time a charge is deposited on a strip, the APV response is simulated. This is linear for low APV baselines (small amount of charge from previous interactions remaining on strips), but becomes non-linear for high APV baselines, the net effect is that the charge effectively deposited/read out from a strip is reduced. The sample including this description of the APV25 dynamic gain is referred to as "MC (with APV simulation)". A sample without including this description of the APV25 dynamic gain is shown for comparison, and is referred to as "MC EOY" (End Of Year).

Data samples:

 - Run-2 Legacy ZeroBias proton-proton collision data at $\sqrt(s)$ = 13 TeV

 - Corresponding to an integrated luminosity of $0.44 fb^{-1}$

 - Collected with the old APV settings, between 9th and 13$^{th}$ August 2016 

Monte Carlo Simulation:

  - MinimumBias MC

  - Simulated events are weighted to match the distribution of the number of pileup (PU) interactions in the data. The number of PU interactions in the data in the following figures is obtained from a measurement of the instantaneous luminosity. 

 The distribution of the charge of clusters in the first TIB and TOB layers and first TID and TEC wheels of the Silicon Strip Tracker. The clusters are required to be associated with a reconstructed track. The simulated sample including a description of the APV dynamic gain, "MC (with APV dynamic gain)", gives an improved description of the data compared to the sample without this description, "MC EOY".'
date: '2020/09/21'
tags:
- Run-2
- Strips
- pp
- Collisions
- 2016